<?php

//klasa koja opisuje Korisnika aplikacije
//identifikacijski broj ($id), ime ($name), prezime ($surname), korisničko ime ($user_name), lozinka ($password)
class User
{
	protected $id, $name, $surname, $user_name, $has_registred, $email, $reg_seq, $password;

	function __construct( $id, $name, $surname, $user_name, $has_registred, $email, $reg_seq, $password )
	{
		$this->id = $id;
		$this->name = $name;
		$this->surname = $surname;
		$this->user_name = $user_name;
		$this->has_registred = $has_registred;
		$this->email = $email;
		$this->reg_seq = $reg_seq;
		$this->password = $password;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>

