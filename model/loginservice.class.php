

<?php


require_once 'db.class.php';

class LoginService
{
	// Obradi login formu, provjeri postoji li korisnik s tom kombinacijom usernama i lozinke u bazi
	// Te ako postoji postavi odgovarajuću SESSION varijablu
	function manageLogin()
	{	
		$mess = [];

		$db = DB::getConnection();
		
		try{
			$st = $db->prepare( 'SELECT id, user_name, has_registered, password FROM users WHERE user_name=:user_name' );
			$st->execute( array( 'user_name' => $_POST['username'] ) );
		}
		catch( PDOException $e ){ exit( 'Greška u bazi prilikom login: ' . $e->getMessage() ); }
		
		$row = $st->fetch();

		if( $row === false ){
			return $mess['msg'] = "Unknown username.";
		}
		else if( isset( $_POST['password'] ) && !password_verify( $_POST['password'], $row['password'] ) ){
			return $mess['msg'] = "Incorrect password or username.";
		}
		else if( $row['has_registered'] === '0' ){
			return $mess['msg'] = "Please confirm your identity by email.";
		}
		else{ //svi podaci su valjani, ulogiraj

			//$_SESSION['user_id'] = $row['id'];
			//echo "user_id in SESSION: " . $_SESSION['user_id'] . "\r\n";
			$mess['username'] = $row['user_name'];
			$mess['user_id'] = $row['id'];
			$mess['msg'] = "success";
			return $mess;
		}
		
	}

	// Obradi signup formu
	function manageSignup()
	{	
		//generiraj registration sequence
		$reg_seq = '';
		for( $i = 0; $i < 30; ++$i )
			$reg_seq .= chr( rand(0, 25) + ord( 'a' ) );

		$db = DB::getConnection();

		try{ // Unesi podatke korisnika u bazu
			$st = $db->prepare( 'INSERT INTO users(name, surname, user_name, has_registered, email, reg_seq, password)' .
								'VALUES (:name, :surname, :user_name, 0, :email, :reg_seq, :password)' );
			$st->execute( array( 'name' => $_POST['name'],
								 'surname' => $_POST['surname'],
								 'user_name' => $_POST['username'],
								 'email' => $_POST['email'],
								 'reg_seq' => $reg_seq,
								 'password' =>  password_hash( $_POST['password'], PASSWORD_DEFAULT )
				) );
		}
		catch( PDOException $e ){ exit( 'Greška u bazi prilikom signup: ' . $e->getMessage() ); }


		//pošalji mail za potvrdu registracije:
		$to = $_POST['email'];
		$subject = 'AppZaCuvanjeBiljeski - registration mail';
		$message = 'Dear ' . $_POST['name'] . ',\n by clicking on the link bellow you will finish your registration:' . "\n" .
				   'http://' . $_SERVER['SERVER_NAME'] . htmlentities( dirname( $_SERVER['PHP_SELF'] ) ) . '/register_route.php?rt=register&reg_seq_back=' . $reg_seq . "\n";
		$headers = 'From: rp2@studenti.math.hr' . "\r\n" .
		            'Reply-To: rp2@studenti.math.hr' . "\r\n" .
		            'X-Mailer: PHP/' . phpversion();

		$potvrda = mail( $to, $subject, $message, $headers );

		if( !$potvrda )
			return "Error while sending registration mail";

		return "success";
	}

	//provjerava jedinstvenost korisnickog imena
	function isUniqueUsername()
	{
		$db = DB::getConnection();

		try{
			$st = $db->prepare( 'SELECT id FROM users WHERE user_name=:user_name' );
			$st->execute( array( 'user_name' => $_POST['username'] ) );
		}
		catch( PDOException $e ){ exit( 'Greška u bazi prilikom provjere username: ' . $e->getMessage() ); }


		if( $st->rowCount() !== 0 )
			return false;
		return true;
	}

	//provjerava jedinstvenost email adrese
	function isUniqueEmail()
	{
		$db = DB::getConnection();

		try{
			$st = $db->prepare( 'SELECT id FROM users WHERE email=:email' );
			$st->execute( array( 'email' => $_POST['email'] ) );
		}
		catch( PDOException $e ){ exit( 'Greška u bazi prilikom provjere email: ' . $e->getMessage() ); }

		if( $st->rowCount() !== 0 )
			return false;
		return true;
	}

	//funkcija koja provjerava registration sequence
	function testRegSeq() 
	{
		if( !isset( $_GET['reg_seq_back'] ) )
			return "Povratni reg_seq nije primljen.";
		if( !preg_match( '/^[a-z]{30}$/' , $_GET['reg_seq_back'] ) )
			return "Registration sequence is not valid.";

		$db = DB::getConnection();

		try{
			$st = $db->prepare( 'SELECT * FROM users WHERE reg_seq=:reg_seq' );
			$st-> execute( array( 'reg_seq' => $_GET['reg_seq_back'] ) );
		}
		catch( PDOException $e ){  exit( 'Greška u bazi prilikom provjere reg_seq: ' . $e->getMessage() ); }


		if( $st->rowCount() === 0 )
			return "No such user with received registration sequence.";
		if( $st->rowCount() > 1 )
			return "Multiple users with equal registration sequence.";

		//samo je jedan
		try{
			$st = $db->prepare( 'UPDATE users SET has_registered=1 WHERE reg_seq=:reg_seq' );
			$st->execute( array( 'reg_seq' => $_GET['reg_seq_back'] ) );
		}
		catch( PDOException $e ){  exit( 'Greška u bazi prilikom ažuriranja reg_seq: ' . $e->getMessage() ); }

		return "success";
	}
}

?>