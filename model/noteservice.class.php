<?php

require_once 'db.class.php';
require_once 'user.class.php';
require_once 'note.class.php';

class NoteService
{
	function getUserById( $id )
	{
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT id, name, surname, user_name, has_registered, email, reg_seq, password FROM users WHERE id=:id' );
			$st->execute( array( 'id' => $id ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$row = $st->fetch();
		if( $row === false )
			return null;
		else
			return new User( $row['id'], $row['name'], $row['surname'], $row['user_name'], $row['has_registered'], $row['email'], $row['reg_seq'], $row['password'] );
	}

	function getUserByUsername( $username )
	{
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT id, name, surname, user_name, has_registered, email, reg_seq, password FROM users WHERE user_name=:user' );
			$st->execute( array( 'user' => $username ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$row = $st->fetch();
		if( $row === false )
			return null;
		else
			return new User( $row['id'], $row['name'], $row['surname'], $row['user_name'], $row['has_registered'], $row['email'], $row['reg_seq'], $row['password'] );
	}

	function getAllUsers( )
	{
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT id, name, surname, user_name, has_registered, email, reg_seq, password FROM users ORDER BY surname' );
			$st->execute();
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$arr = array();
		while( $row = $st->fetch() )
		{
			$arr[] = new User( $row['id'], $row['name'], $row['surname'], $row['user_name'], $row['has_registered'], $row['email'], $row['reg_seq'], $row['password'] );
		}

		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------

	function getNoteById( $id )
	{
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT id, user_id, title, content, timeDate FROM notes WHERE id=:id' );
			$st->execute( array( 'id' => $id ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$row = $st->fetch();
		if( $row === false )
			return null;
		else{
			$note = new Note( $row['id'], $row['user_id'], $row['title'], $row['content'], $row['timeDate'] );
			return $note->serial();

		}
	}

	function getNotesByUserId( $user_id )
	{
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT DISTINCT id, user_id, title, content, timeDate FROM notes, shared WHERE user_id=:user_id OR shared_user_id=:user_id ORDER BY timeDate' );
			$st->execute( array( 'user_id' => $user_id ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$arr = array();
		while( $row = $st->fetch() )
		{
			$note = new Note( $row['id'], $row['user_id'], $row['title'], $row['content'], $row['timeDate'] );
			$arr[] = $note->serial();
		}

		return $arr;
	}

	//Koristimo jedan search
	//Ovo je za sada kada je barem jedan tag
	function getNotesByTagOrContent( $user_id, $element )
	{
			$parts = explode(' ', $element);
			$tags = implode(', ', $parts);
			$string = '%' . $element . '%';
			$arr = [];
			try
			{
				$db = DB::getConnection();
				$st = $db->prepare( 'SELECT DISTINCT id, user_id, title, content, timeDate
															FROM notes, shared, tag
															WHERE (content LIKE :cont OR title LIKE :cont) AND (user_id=:user_id OR (shared.note_id = id AND shared_user_id=:user_id) )
																		OR id IN
																				(SELECT id FROM notes, shared, tag
																					WHERE name IN (:name) AND (user_id=:user_id OR (shared.note_id = id AND shared_user_id=:user_id) )
																					GROUP BY tag.note_id HAVING count(*)>= :len)
															ORDER BY timeDate' );
				$st->execute( array( 'name' => $tags, 'cont' => $string, 'user_id' => $user_id, 'len' => count($parts)) );
			}
			catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

			while( $row = $st->fetch() )
			{
				$note = new Note( $row['id'], $row['user_id'], $row['title'], $row['content'], $row['timeDate'] );
				$arr[] = $note->serial();
			}


		return $arr;
	}

	function getAllNotes()
	{
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT id, user_id, title, content, timeDate FROM notes ORDER BY timeDate' );
			$st->execute();
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$arr = array();
		while( $row = $st->fetch() ){
			$note = new Note( $row['id'], $row['user_id'], $row['title'], $row['content'], $row['timeDate'] );
			$arr[] = $note->serial();
		}

		return $arr;
	}


//-------------------------------------------------------------------------------------------------------------------------
// Dodavanje biljeski

//nije nužno da svaka bilješka ima tag
//$tag je polje
function addNote( $user_id, $title, $content, $tag = "" ){
	try
	{
		$db = DB::getConnection();
		$st = $db->prepare( 'INSERT INTO notes(user_id, title, content) VALUES (:user_id, :title, :content)' );
		$st->execute( array( 'user_id' => $user_id, 'title' => $title, 'content' => $content ) );

		//posljedji unesen id
		$st = $db->prepare( 'SELECT LAST_INSERT_ID()' );
		$st->execute();

		$row = $st->fetch();
		$last_id = $row['LAST_INSERT_ID()'];

		$parts = explode(' ', $tag);

		foreach ($parts as $value) {
			$st = $db->prepare( 'INSERT INTO tag(name, note_id) VALUES (:name, :note_id)' );
			$st->execute( array( 'name' => $value, 'note_id' => $last_id ) );
		}
	}
	catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }
}

function shareWith( $note_id, $user_id ){
	try
	{
		$db = DB::getConnection();
		$st = $db->prepare( 'INSERT INTO shared(note_id, shared_user_id)
													SELECT :note_id, :user_id
													FROM notes WHERE id=:note_id AND user_id!=:user_id' );
		$st->execute( array( 'note_id' => $note_id, 'user_id' => $user_id ) );
	}
	catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }
}

function addTag( $note_id, $name ){
	try
	{
		$db = DB::getConnection();
		$st = $db->prepare( 'INSERT INTO tag(name, note_id) VALUES (:name, :note_id)' );
		$st->execute( array( 'name' => $name, 'note_id' => $note_id ) );
	}
	catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }
}

//-------------------------------------------------------------------------------------------------------------------------
// Brisanje biljeski

//Stjepan:
//Samo vlasnik moze izbrisati biljesku
	function removeNote( $note_id, $user_id){
		try
		{
			$db = DB::getConnection();

			$st = $db->prepare( 'DELETE FROM tag
														WHERE note_id=:id AND note_id = (SELECT id FROM notes WHERE id=:id AND user_id=:user_id)' );
			$st->execute( array('id' => $note_id, 'user_id' => $user_id) );

			//Ako je vlasnik brise sve sharove
			$st = $db->prepare( 'DELETE FROM shared
														WHERE note_id=:id AND note_id = (SELECT id FROM notes WHERE id=:id AND user_id=:user_id)' );
			$st->execute( array('id' => $note_id, 'user_id' => $user_id) );

			$st = $db->prepare( 'DELETE FROM notes WHERE id=:id AND user_id=:user_id' );
			$st->execute( array('id' => $note_id, 'user_id' => $user_id) );

			//Brise samo share ako nije vlasnik
			$st = $db->prepare( 'DELETE FROM shared WHERE note_id=:id AND shared_user_id=:user_id' );
			$st->execute( array('id' => $note_id, 'user_id' => $user_id) );


		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

	}

	function removeSharedNoteForAll( $note_id, $user_id ){
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'DELETE FROM shared WHERE note_id=:id' );
			$st->execute( array('id' => $note_id ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

	}

	function removeSharedNoteForUser( $note_id, $user_id ){
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'DELETE FROM shared WHERE note_id=:id AND shared_user_id=:user_id' );
			$st->execute( array('id' => $note_id, 'user_id' => $user_id) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

	}

	function removeTagForNote( $note_id, $tag ){
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'DELETE FROM tag WHERE note_id=:id AND name=:name' );
			$st->execute( array('id' => $note_id, 'name' => $tag ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

	}

//-------------------------------------------------------------------------------------------------------------------------
//Izmjene biljeske

	function changeNote($note_id, $user_id, $title, $content){
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'UPDATE notes SET user_id=:user_id, title=:title, content=:content WHERE id=:note_id' );
			$st->execute( array( 'note_id' => $note_id, 'user_id' => $user_id, 'title' => $title, 'content' => $content ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

	}

//samo za tekst, tagovi i share idu preko add remove
//-------------------------------------------------------------------------------------------------------------------------
//Info

	function isShared( $note_id ){
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT note_id FROM shared WHERE note_id=:id' );
			$st->execute( array('id' => $note_id ) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		$row = $st->fetch();
		if($row === false){
			return false;
		}
		else return true;
	}

	function canShare( $note_id, $user_id ){
		try
		{
			$db = DB::getConnection();
			$st = $db->prepare( 'SELECT id FROM notes WHERE id=:id AND user_id=:user_id' );
			$st->execute( array('id' => $note_id, 'user_id' => $user_id) );
		}
		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

		if( $st->fetch() ){
			return true;
		}
		else return false;
	}
};

?>
