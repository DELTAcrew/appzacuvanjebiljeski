<?php

//klasa koja opisuje Bilješku
//identifikacijski broj ($id), identifikacijski broj korisnika kojem pripada bilješka ($user_id),
//naslov bilješke ($title), sadržaj bilješke ($content), vrijeme i datum bilješke ($timeDate)(timestamp)
class Note
{
	protected $id, $user_id, $title, $content, $timeDate;

	function __construct( $id, $user_id, $title, $content, $timeDate )
	{
		$this->id = $id;
		$this->user_id = $user_id;
		$this->title = $title;
		$this->content = $content;
		$this->timeDate = $timeDate;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }

	function serial(){
		return array('id' => $this->id,
									'user_id' => $this->user_id,
									'title' => $this->title,
									'content' => $this->content,
									'timeDate' => $this->timeDate);
	}
}

?>
