<?php
session_start();
?>

<?php
/*
	echo '$_SESSION:'; print_r( $_SESSION ); echo "<br />";
	echo '$_POST:'; print_r( $_POST ); echo "<br />";
	echo '$_GET:'; print_r( $_GET ); echo "<br />";
	echo "<br /><br />";
	*/
?>


<!DOCTYPE>
<html>
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width = device-width, initial-scale = 1">
	<title>AppZaCuvanjeBiljeski</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>
	<div class="container">
	<div class="page-header">
		<h1>Notes</h1>
	</div>	

	<div class="jumbotron">
	<button id="button_login" class="btn btn-default btn-lg">Login</button>
	<button id="button_signup" class="btn btn-danger btn-lg">Signup</button>
	</div>

	<div class="jumbotron">
	<div id="div_login">
	Login to account: <br />
	<form method="post" id="form_login">
		Username <input id="login_username" type="text" name="username" /><br />
		Password <input id="login_password" type="password"name="password" /><br />
		<input type="submit" value="Login" id="login" class="btn btn-info"/><br />
	</form>
	<div id="login_message"></div>
	</div>

	<br />
	<br />

	<div id="div_signup">
	<div id="div_signup_form">
	Signup: <br />
	<form method="post" id="form_signup">
		Name <input id="name" type="text" name="name" /><br />
		Surname <input id="surname" type="text" name="surname" /><br />
		Username <input id="signup_username" type="text" name="username" /><br />
		Email <input id="email" type="text" name="email" /><br />
		Password <input id="signup_password" type="password"name="password" /><br />
		<input type="submit" value="Signup" id="signup" class="btn btn-info"/>
	</form>
	</div>
	</div>
	</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="login.js"></script>
</body>
</html>
