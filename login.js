
$( "#div_login" ).hide();
$( "#div_signup" ).hide();


$( document ).ready( function()
{
	var login_flag = 0, signup_flag = 0;

	if( sessionStorage.getItem( 'user_id' ) !== null ){
		window.location.href = 'prikaz_biljeski.html';
	}

	$( "#button_login" ).on( "click" , function(){
		if( login_flag === 0 ){
			$( "#button_login" ).html( 'Back' );
			$( "#button_signup" ).hide();
			$( "#div_login" ).show();
			login_flag = 1;
		}
		else
		{	
			$( "#button_login" ).html( 'Login' );
			$( "#button_signup" ).show();
			$( "#div_login" ).hide();
			login_flag = 0;
		}
	});

	$( "#button_signup" ).on( "click" , function(){
		if( signup_flag === 0 ){
			$( "#button_signup" ).html( 'Back' );
			$( "#button_login" ).hide();
			$( "#div_signup" ).show();
			signup_flag = 1;
		}
		else
		{
			$( "#button_signup" ).html( 'Signup' );
			$( "#button_login" ).show();
			$( "#div_signup" ).hide();
			signup_flag = 0;
		}
	})


	$( "#login" ).on( "click" , function( event )
		{
			event.preventDefault();

			$.ajax(
			{
				url: 'controller/Controller.php',
				data:
				{
					rt: 'login/index',
					username: $( "#login_username" ).val(),
					password: $( "#login_password" ).val()
				},
				type: 'POST',
				success: function( data ){ 
					console.log( "Successful click - login" );
					var poruka = $( '<div id="poruka_login"></div>' );
					poruka.html( data.error );

					$( "#login_message" ).html( '' );
					$( "#login_message" ).append( poruka );
					if( data.error === "Login was successful" )
					{
						sessionStorage.setItem( 'username' , data.username );
						sessionStorage.setItem( 'user_id' , data.user_id );
						window.location.href = 'prikaz_biljeski.html';
					}
				},
				error:  function( xhr, status ){
					console.log( "Greška ajax upit za login. " + status );
				}
			});
		});
	

	$( "#signup" ).on( "click" , function( event ) 
	{
		event.preventDefault();

		$.ajax(
		{
			url: 'controller/Controller.php',
			data:
			{
				rt: 'signup/index',
				name: $( "#name" ).val(),
				surname: $( "#surname" ).val(),
				username: $( "#signup_username" ).val(),
				email: $( "#email" ).val(),
				password: $( "#signup_password" ).val()
			},
			type: 'POST',
			success: function( data ){
				console.log( "Successful click - signup" );
				var poruka = $( '<div id="poruka_signup"></div>' );
				poruka.html( data.error );

				if( data.error === "Signup was successful. Check your mailbox." ){
					$( "#div_signup_form" ).hide();
					setTimeout(	function(){ window.location.href = 'index.php'; }, 5000);
				}
				$( "#div_signup" ).append( poruka );
			},
			error: function( xhr, status ){
					console.log( "Greška ajax upit za signup. " + status );
			}
		});
	})


});