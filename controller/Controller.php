<?php
//Ovo je alternativa ruteru
// Zelimo da ajax poziva ovaj program. Ajax ce uvijek slati 'rt' iz kojeg
// vidimo koji se kontroler i koja funkcija moraju pozivati.
// Na kraju izvrsavanja funkcije umjesto ispisa poiva se slanje poruke
// U ajaxu ocekujemo:
// url = controler/Contorler.php //to bi trebalo raditi
// data = {
//  //obavezni
//  rt = 'controler/akcija'
//   user_id = ''
//   //opcijonalni ovisno o funkciji
//  note_id
//  user_name
//  search //ovo je za pretragu po sadrzaju u tagu
//  title
//  content
//  tag
// }
// method = post
// return message
//    error//uvijek postavljen
//          200 OK, 404 Nema prikaza stranice, 405 Nema Usera s tim imenom
//          300 podijeljena je biljeska, 301 nije podijelnjena
//          302 moze podijeliti 303 nemoze podijeliti
//    note//ona se javlja u funckiji search kada possaljemo vrijednost note_id
//    noteList


//Funkcija za odgovor
function sendJSONandExit( $message ){
  header( 'Content-type:application/json;charset=utf-8' );
  echo json_encode( $message );
  flush();
  exit( 0 );
}


// Klasicni ruter
if( isset( $_POST['rt'] ) )
	$route = $_POST['rt'];
else
	$route = 'index';

// Ako je $route == 'con/act', onda rastavi na $controllerName='con', $action='act'
$parts = explode( '/', $route );

$controllerName = $parts[0] . 'Controller';
if( isset( $parts[1] ) )
	$action = $parts[1];
else
	$action = 'index';

// Controller $controllerName se nalazi poddirektoriju controller
$controllerFileName = $controllerName . '.php';

// Includeaj tu datoteku
if( !file_exists( $controllerFileName ) )
{
	$controllerName = '_404Controller';
	$controllerFileName = $controllerName . '.php';
}

require_once $controllerFileName;

// Stvori pripadni kontroler
$con = new $controllerName;

// Ako u njemu nema tražene akcije, stavi da se traži akcija index
if( !method_exists( $con, $action ) )
	$action = 'index';

// Pozovi odgovarajuću akciju, u akcijama punimo message
$con->$action();
//sendJSONandExit($message);
?>
