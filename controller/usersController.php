<?php 

require_once 'model/noteservice.class.php';

class UsersController
{
	public function index() 
	{
		$ls = new NoteService();

		$title = 'Popis svih korisnika';
		$userList = $ls->getAllUsers();

		require_once 'view/users_index.php';
	}
}; 

?>
