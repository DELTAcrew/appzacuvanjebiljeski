
<?php
session_start();

require_once '../model/loginservice.class.php';

class registerController
{
	public function index()
	{
		$message = [];
		$message['error'] = 200;


		$ls = new loginservice();
		$potvrda = $ls->testRegSeq(); // Provjeri registration sequence

		if( $potvrda === "success" )
		{
			$message['error'] = "Registration was successful.";
			header( 'Location: ../index.php' );
			exit();
		}
		else
		{
			$message['error'] = $potvrda;
			header( 'Location: ../registration_failed.php' );
			exit();
		}

	}
}
?>