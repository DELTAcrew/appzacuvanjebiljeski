

<?php
session_start();

require_once '../model/loginservice.class.php';

class loginController
{
	public function index()
	{
		$message = [];
		$message['error'] = 200;


		// Provjeri jesu li uneseni svi podaci u login formu
		if( !isset( $_POST['username'] ) || !isset( $_POST['password'] ) )
		{
			$message['error'] = "Treba unjeti username i password.";
			sendJSONandExit($message);
		}

		
		$ls = new LoginService();

		// Obradi login formu
		$potvrda = $ls->manageLogin();
		if( $potvrda['msg'] === "success" ){
			$message['user_id'] = $potvrda['user_id'];
			$message['username'] = $potvrda['username'];
			$message['error'] = "Login was successful";
		}
		else{
			$message['error'] = $potvrda;
		}
		sendJSONandExit($message);
	}


}
?>