


<?php
session_start();

require_once '../model/loginservice.class.php';

class signupController
{
	public function index()
	{
		$message = [];
		$message['error'] = 200;

		//Provjeri je li korisnik ispunio sve podatke
		if( !isset( $_POST['name'] ) || !isset( $_POST['surname'] ) || !isset( $_POST['username'] ) || !isset( $_POST['email'] ) || !isset( $_POST['password'] ) )
		{
			$message['error'] = "All informations must be provided.";
			sendJSONandExit($message);
		}
		
		if( !filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL) ) //Provjeri je li unesena smislena email adresa
		{
			$message['error'] = "Email is not valid.";
			sendJSONandExit($message);
		}


		$ls = new LoginService();

		if( !$ls->isUniqueUsername() ) //Provjeri postoji li vec korisnik s tim usernameom
		{				
			$message['error'] = "Username is taken.";
			sendJSONandExit($message);
		}
		
		if( !$ls->isUniqueEmail() ) //Provjeri postoji li vec korinsik s tom email adresom
		{
			$message['error'] = "Email is taken.";				
			sendJSONandExit($message);
		}
	
		//Ako je sve prošlo u redu, obradi formu i unesi korisnika u bazu
		$potvrda = $ls->manageSignup(); 
		if( $potvrda === "success" ){
			$message['error'] = "Signup was successful. Check your mailbox.";
		}
		else{	//ukoliko signup nije bio uspješan
			$message['error'] = $potvrda;
		}			
		sendJSONandExit($message);


	}
}
?>