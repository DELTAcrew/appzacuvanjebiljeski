<?php

require_once 'model/noteservice.class.php';

class _404Controller
{
	public function index()
	{
		$message['error'] = '404';
		sendJSONandExit($message);
	}
};

?>
