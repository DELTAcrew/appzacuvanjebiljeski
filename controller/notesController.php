<?php

require_once '../model/noteservice.class.php';

class NotesController
{
	public function index()
	{
		$ns = new NoteService();

		$message = [];
		$message['error'] = '200';
		$message['noteList'] = $ns->getNotesByUserId( $_POST['user_id'] );
		sendJSONandExit($message);

	}


	public function search()
	{
		//Stjepan
		//ovo ce biti visenamjenska funkcija
		//ovisno o postavljenim varijablama posta poziva se druga funckija i salje drugaciji odgovor
		//mozemo traziti po:
		//	Tagovima i sadrzaju
		//  id-u biljeske
		//	ili svih biljeski od zadanog korisnika, ako nema uvjeta za pretragu
		//	ekvivalentno indexu u tom slucaju
		$ns = new NoteService();

		$message = [];
		$message['error'] = '200';

		//searchTag
		//prima string
		if(isset( $_POST['search']) ){
			$noteList = [];

			$noteList = $ns->getNotesByTagOrContent( $_POST['user_id'], $_POST['search'] );

			$message['noteList'] = $noteList;
			sendJSONandExit($message);
		}

		//moglo bi biti od koristi kada cemo izmjenivati
		if( isset( $_POST['note_id']) ){
			$message['note'] = $ns->getNoteById( $_POST['note_id'] );
			sendJSONandExit($message);
		}

		$message['noteList'] = $ns->getNotesByUserId( $_POST['user_id'] );
		sendJSONandExit($message);


	}

//-------------------------------------------------------------------------------------------------------------------------

	public function newNote(){
		 $ns = New NoteService();
		 $message = [];
		 $message['error'] = '200';

		 if( isset( $_POST['tag']) ){
			 $ns->addNote($_POST['user_id'], $_POST['title'], $_POST['content'], $_POST['tag']);
			}
			else {
				$ns->addNote($_POST['user_id'], $_POST['title'], $_POST['content']);
			}
			sendJSONandExit($message);

	}

	public function shareNote(){
			$ns = New NoteService();
			$username = $ns->getUserByUsername($_POST['user_name']);

			$message = [];
			$message['error'] = '200';

			if( is_null($username) ){
				$message['error'] = '405';
				sendJSONandExit($message);
			}

			$ns->shareWith( $_POST['note_id'], $username->id );
			sendJSONandExit($message);

	}

	//Provjerava dali je bilješka podijeljena
	//Pomaze za share/unshere odabir
	public function isShared(){
			$ns = New NoteService();
			$message = [];
			$message['error'] = '200';

			if( $ns->isShared($_POST['note_id']) )
				$message['error'] = '300';
			else
				$message['error'] = '301';
			sendJSONandExit($message);
	}

	//Ako nije vlasnik ne moze podijeliti
	//Pomoc za provjeru treba li opcija share note
	public function canShare(){
		$ns = New NoteService();
		$message = [];
		$message['error'] = '200';

		if( $ns->canShare( $_POST['note_id'], $_POST['user_id'] ) )
			$message['error'] = '302';
		else
			$message['error'] = '303';

		sendJSONandExit($message);

	}


	public function newTag(){
		$ns = New NoteService();
		$message = [];
		$message['error'] = '200';

		$ns->addTag( $_POST['note_id'], $_POST['tag']);
		sendJSONandExit($message);

	}

//-------------------------------------------------------------------------------------------------------------------------

	public function deleteNote(){
		$ns = New NoteService();
		$message = [];
		$message['error'] = '200';

		$ns->removeNote( $_POST['note_id'], $_POST['user_id']);
		sendJSONandExit($message);

	}

	public function deleteTag(){
		$ns = New NoteService();
		$message = [];
		$message['error'] = '200';

		$ns->removeTagForNote( $_POST['note_id'], $_POST['tag']);
		sendJSONandExit($message);

	}

	//jedino vlasnik moze pozvati
	public function unshareNote(){
		$ns = New NoteService();
		$message = [];
		$message['error'] = '200';

		$ns->removeSharedNoteForAll( $_POST['note_id'], $_POST['user_id']);
		sendJSONandExit($message);

	}

//-------------------------------------------------------------------------------------------------------------------------

	public function updateNote(){
		$ns = New NoteService();
		$message = [];
		$message['error'] = '200';

		$ns->changeNote($_POST['note_id'], $_POST['user_id'], $_POST['title'], $_POST['content']);
		sendJSONandExit($message);

	}

};

?>
