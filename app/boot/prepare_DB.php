<?php

// Manualno inicijaliziramo bazu ako već nije.
require_once '../../model/db.class.php';

$db = DB::getConnection();

try
{
	//Kreiraj tablicu 'users'
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS users (' .
		'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
		'name varchar(50) NOT NULL,' .
		'surname varchar(50) NOT NULL,' .
		'user_name varchar(50) NOT NULL,' .
		'has_registered int NOT NULL,' .
		'email varchar(50) NOT NULL,' .
		'reg_seq varchar(50) NOT NULL,' .
		'password varchar(255) NOT NULL)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error #1: " . $e->getMessage() ); }

echo "Napravio tablicu users.<br />";


try
{
	//Kreiraj tablicu 'notes'
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS notes (' .
		'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
		'user_id int NOT NULL,' .
		'title varchar(50) NOT NULL,' .
		'content text NOT NULL,' .
		'timeDate timestamp DEFAULT CURRENT_TIMESTAMP)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error #2: " . $e->getMessage() ); }

echo "Napravio tablicu notes.<br />";


try
{
	//Kreiraj tablicu 'shared'
	//sluzi da znamo s kim je podjeljena jos
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS shared (' .
		'note_id int NOT NULL ,' .
		'shared_user_id int NOT NULL)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error #3: " . $e->getMessage() ); }

echo "Napravio tablicu shared.<br />";


try
{
	//Kreiraj tablicu 'tag'
	//id za tag nam nije potreban
	//pamtimo samo kojoj je noti pridruzena
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS tag (' .
		'name varchar(20) NOT NULL ,' .
		'note_id int NOT NULL)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error #4: " . $e->getMessage() ); }

echo "Napravio tablicu tag.<br />";

// Ubaci neke korisnike unutra
try
{
	$st = $db->prepare( 'INSERT INTO users(name, surname, user_name, has_registered, email, reg_seq, password) VALUES (:name, :surname, :user_name, :has_registered, :email, :reg_seq, :password)' );

	$st->execute( array( 'name' => 'Pero', 'surname' => 'Perić', 'user_name' => 'pperic', 'has_registered' => '1', 'email' => 'pperic@nijemail.com', 'reg_seq' => '111', 'password' => password_hash( 'perinasifra', PASSWORD_DEFAULT ) ) );
	$st->execute( array( 'name' => 'Mirko', 'surname' => 'Mirić', 'user_name' => 'mmiric', 'has_registered' => '1', 'email' => 'mmiric@nijemail.com', 'reg_seq' => '222', 'password' => password_hash( 'mirkovasifra', PASSWORD_DEFAULT ) ) );
	$st->execute( array( 'name' => 'Slavko', 'surname' => 'Slavić', 'user_name' => 'sslavic', 'has_registered' => '1', 'email' => 'slavic@nijemail.com', 'reg_seq' => '333', 'password' => password_hash( 'slavkovasifra', PASSWORD_DEFAULT ) ) );
	$st->execute( array( 'name' => 'Ana', 'surname' => 'Anić', 'user_name' => 'aanic', 'has_registered' => '1', 'email' => 'aanic@nijemail.com', 'reg_seq' => '444', 'password' => password_hash( 'aninasifra', PASSWORD_DEFAULT ) ) );
	$st->execute( array( 'name' => 'Maja', 'surname' => 'Majić', 'user_name' => 'mmajic', 'has_registered' => '1', 'email' => 'mmajic@nijemail.com', 'reg_seq' => '555', 'password' => password_hash( 'majinasifra', PASSWORD_DEFAULT ) ) );
}
catch( PDOException $e ) { exit( "PDO error #5: " . $e->getMessage() ); }

echo "Ubacio korisnike u tablicu users.<br />";



// Ubaci neke bilješke unutra
try
{
	$st = $db->prepare( 'INSERT INTO notes(user_id, title, content) VALUES (:user_id, :title, :content)' );

	$st->execute( array( 'user_id' => '2', 'title' => 'Mirkova bilješka 1', 'content' => 'Mirkova prva bilješka, to je također prva bilješka u bazi' ) );
	$st->execute( array( 'user_id' => '2', 'title' => 'Mirkova bilješka 2', 'content' => 'Mirkova druga bilješka ..... xx.xx. čestitaj Ani rođemdan' ) );
	$st->execute( array( 'user_id' => '1', 'title' => 'Perina bilješka 1', 'content' => 'Perina prva bilješka. Napravi projekt iz rp2' ) );
	$st->execute( array( 'user_id' => '4', 'title' => 'Anina bilješka 1', 'content' => 'Anina prva bilješka: pogledaj neki dobar film' ) );
	$st->execute( array( 'user_id' => '1', 'title' => 'Perina bilješka 2', 'content' => 'Perina druga bilješka. Završi projekt iz rp2' ) );
	$st->execute( array( 'user_id' => '2', 'title' => 'Mirkova bilješka 3', 'content' => 'Mirkova treća bilješka ..... kupi u dućanu ....' ) );
	$st->execute( array( 'user_id' => '5', 'title' => 'Majina bilješka 1', 'content' => 'Majina prva bilješka: dogovori termin kod doktora idući tjedan' ) );

}
catch( PDOException $e ) { exit( "PDO error #6: " . $e->getMessage() ); }

echo "Ubacio bilješke u tablicu notes.<br />";

// Ubaci neke podijeljene biljeske
try
{
	$st = $db->prepare( 'INSERT INTO shared(note_id, shared_user_id) VALUES (:note_id, :shared_user_id)' );

	$st->execute( array( 'note_id' => '2', 'shared_user_id' => '5' ) );
	$st->execute( array( 'note_id' => '7', 'shared_user_id' => '3' ) );
	$st->execute( array( 'note_id' => '1', 'shared_user_id' => '3' ) );

}
catch( PDOException $e ) { exit( "PDO error #7: " . $e->getMessage() ); }

echo "Ubacio podjeljene bilješke u tablicu shared.<br />";



// Ubaci neke tagove
try
{
	$st = $db->prepare( 'INSERT INTO tag(name, note_id) VALUES (:name, :note_id)' );
	$st->execute( array( 'name' => 'prva', 'note_id' => '1' ) );
	$st->execute( array( 'name' => 'prva', 'note_id' => '3' ) );
	$st->execute( array( 'name' => 'prva', 'note_id' => '4' ) );
	$st->execute( array( 'name' => 'prva', 'note_id' => '7' ) );
	$st->execute( array( 'name' => 'vazno', 'note_id' => '2' ) );

}
catch( PDOException $e ) { exit( "PDO error #8: " . $e->getMessage() ); }

echo "Ubacio tagove u tablicu tag.<br />";



?>
