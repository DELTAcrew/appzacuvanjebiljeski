<?php require_once 'view/_header.php'; ?>

<table>
	<tr><th>Naslov</th><th>Sadržaj</th></tr>
	<?php 
		foreach( $noteList as $note )
		{
			echo '<tr>' .
			     '<td>' . $note->title . '</td>' .
			     '<td>' . $note->content . '</td>' .
			     '</tr>';
		}
	?>
</table>

<?php require_once 'view/_footer.php'; ?>
